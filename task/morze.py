def code_morze(value):
    '''
    please add your solution here or call your solution implemented in different function from here
    then change return value from 'False' to value that will be returned by your solution
    '''

    codes = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....',
             'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--',
             'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-',
             'W': '.--', 'X': '-..-',
             'Y': '-.--', 'Z': '--..',
             '1': '.----', '2': '..---', '3': '...--',
             '4': '....-', '5': '.....', '6': '-....',
             '7': '--...', '8': '---..', '9': '----.', '0': '-----', ', ': '--..--', '.': '.-.-.-', '?': '..--..',
             '/': '-..-.',
             '-': '-....-', '(': '-.--.', ')': '-.--.-'}

    value = value.upper()
    morse_string = ""

    for char in value:
        for key, value in codes.items():
            if char == " ":
                continue
            elif char in key:
                morse_string = morse_string + value + " "
                break

    morse_string = morse_string.strip()

    return morse_string